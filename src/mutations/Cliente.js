import gql from 'graphql-tag';

export  const CrearCliente =  gql `
mutation crearCliente($input: ClienteInput){
    crearCliente(input: $input){
      id
      nombre
      apellido
    }
}`;
  