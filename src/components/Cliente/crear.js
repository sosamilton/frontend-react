import React, { Component, Fragment } from 'react';
import { CrearCliente } from '../../mutations/Cliente';
import { Mutation } from 'react-apollo';

class CrearClientes extends Component{

    state={
        cliente:{
            nombre: "",
            apellido:"",
            email: "",
            tipoCliente: ""
        }
    }

    render(){
        return(
            <Fragment>
                <h2 className="text-center"> Crear Cliente </h2>
                <div className="row justify-content-center">
                    <Mutation mutation={CrearCliente}>
                    { crearCliente => (
                        <form 
                            className="col-md-8 m-3"
                            onSubmit={e => {
                                e.preventDefault();
                                const { nombre, apellido, tipoCliente,email } = this.state.cliente;

                                //no se pone clave y valor cuando es el mismo nombre, en el caso de ser numero hay que especificarlo porque los datos del form lo toma siempre como string ej: edad: Number(edad),
                                const input = {
                                    nombre,
                                    apellido,
                                    tipoCliente,
                                    email,
                                }
                                crearCliente({
                                    variables: {input}
                                })
                            }} 
                        >
                            <div className="form-row">
                                <div className="form-group col-md-6">
                                    <label>Nombre</label>
                                    <input 
                                        type="text" 
                                        className="form-control" 
                                        placeholder="Nombre"
                                        onChange={e => {
                                            this.setState({
                                                cliente:{
                                                    ...this.state.cliente,
                                                    nombre: e.target.value
                                                }
                                            })
                                        }}
                                    />
                                </div>
                                <div className="form-group col-md-6">
                                    <label>Apellido</label>
                                    <input 
                                        type="text" 
                                        className="form-control" 
                                        placeholder="Apellido"
                                        onChange={e => {
                                            this.setState({
                                                cliente:{
                                                    ...this.state.cliente,
                                                    apellido: e.target.value
                                                }
                                            })
                                        }}
                                    />
                                </div>
                                <div className="form-group col-md-6">
                                    <label>Email</label>
                                    <input 
                                        type="email" 
                                        className="form-control" 
                                        placeholder="Email" 
                                        onChange={e => {
                                            this.setState({
                                                cliente:{
                                                    ...this.state.cliente,
                                                    email: e.target.value
                                                }
                                            })
                                        }}
                                    />
                                </div>
                                <div className="form-group col-md-6">
                                    <label>Tipo Cliente</label>  
                                    <select 
                                        className="form-control"
                                        onChange={e => {
                                            this.setState({
                                                cliente:{
                                                    ...this.state.cliente,
                                                    tipoCliente: e.target.value
                                                }
                                            })
                                        }}
                                        >
                                        <option value="">Elegir...</option>
                                        <option value="Full">Full</option>
                                        <option value="Basico">Basico</option>
                                    </select>
                                </div>
                            </div>
                            <button type="submit" className="btn btn-success float-right">Crear Cliente</button>
                        </form>
                    )}
                    </Mutation>
                </div>
            </Fragment>
        );
    }
}

export default CrearClientes