import React, { Fragment } from 'react';
import { Link } from 'react-router-dom';
import { Query } from 'react-apollo';
import { ClienteQuery } from '../../queries/Cliente';

const Clientes = () => (
    <Query query={ClienteQuery} >
        {({ loading, error, data }) => {
            if(loading) return "Cargando..."; 
            if(error) return `Error: ${error.message}`
            console.log(data);
            return(
                <Fragment>
                     <h2 className="text-center"> Listado Clientes</h2>
                     <ul className="list-group">
                         {data.getClientes.map(item => (
                             <li key={item.id} className="list-group-item">
                                 <div className="row align-items-center"> 
                                    <div className="col-md-8">
                                        {item.nombre} {item.apellido} - {item.email}
                                    </div>
                                    <div className="col-md-4">
                                        <Link to={`/client/edit/${item.id}`} className="btn btn-success">
                                            Editar Cliente
                                        </Link>
                                    </div>
                                 </div>
                             </li>
                         ))}                      
                     </ul>
                </Fragment>
            )
        }}
    </Query>
)

export default Clientes